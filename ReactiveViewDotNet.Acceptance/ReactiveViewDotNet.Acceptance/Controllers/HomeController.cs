﻿using System.Web.Mvc;

namespace ReactiveViewDotNet.Acceptance.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new {Id = "HelloWorld", State = 4});
        }
    }
}