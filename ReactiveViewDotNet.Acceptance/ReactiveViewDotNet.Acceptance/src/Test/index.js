﻿import React from 'react';

const names = ["Name1", "Name2", "Name3"];


class Greeting extends React.Component
{
	constructor(props)
	{
	    super(props);

	    this.state = 
	    {
			nameIndex: props.nameIndex
	    };

	    this.handleClick = this.handleClick.bind(this);
	}

	handleClick(e)
	{
    	e.preventDefault();
    	
    	this.setState(
	    	{
    			nameIndex: this.state.nameIndex < names.length - 1 ? this.state.nameIndex + 1 : 0
    		}
		);
  	}

	render()
	{
		return <div>
				<h1>Hello, <a href="#" onClick={this.handleClick}>{names[this.state.nameIndex]}</a></h1>
			</div>;
	}
}

module.exports.createComponent = function (initialState) {
    return <Greeting nameIndex={initialState.nameIndex} />;
};