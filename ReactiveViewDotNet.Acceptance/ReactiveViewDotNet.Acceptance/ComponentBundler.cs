﻿using System;
using System.IO;
using System.Reflection;
using System.Web.Hosting;
using ReactiveViewDotNet.ComponentBundling;
using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet.Acceptance
{
    public static  class ComponentBundler
    {
        public static readonly string PublicFilesLink = @"/Scripts/Components/";
        public static string PublicFilesPath = HostingEnvironment.MapPath("~/Scripts/Components/");
        public static string LibsPath = HostingEnvironment.MapPath("~/Scripts/Libs/");

        private static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public static void BundleComponent(string componentName, IPhysicalFileSystem fileSystem)
        {
            var link = fileSystem.GetFullUrl(string.Format("{0}/index.js", componentName));

            var settings = new BundlingSettings
            {
                ComponentDirectory = HostingEnvironment.MapPath(string.Format("~/src/{0}", componentName)),
                EntryFilename = "index.js",
                ServerOutputDirectory = HostingEnvironment.MapPath(string.Format("~/Views/{0}", componentName)),
                ClientOutputDirectory = HostingEnvironment.MapPath(string.Format("~/Scripts/Components/{0}", componentName)),
                OutputFilename = "index.js",
                ModulesDirectory = Path.Combine(AssemblyDirectory, "node_modules"),
                AfterComponentSourceCode = string.Format("global['ReactiveViews'].setComponent('{0}', module.exports);", link),
                ExternalModules = new[] { "react" }
            };

            new WebpackBundler().Bundle(settings);
        }
        
        public static void BundleLibs()
        {
            var bundlingSettings = new ModulesBundlingSettings
            {
                ComponentsNames = new [] { "react" }, 
                ModulesDirectory = Path.Combine(AssemblyDirectory, "node_modules"),
                OutputDirectory = LibsPath
            };

            new ModulesBundler().Bundle(bundlingSettings);
        }
    }
}