﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet.Acceptance
{
    public class ThisApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            var fs = new PhysicalFileSystem(Server.MapPath("~/Views/"), "/Scripts/Components/", ComponentBundler.PublicFilesPath);

            ViewEngines.Engines.Insert(0, new ReactiveViewEngine(new ReactiveViewEngineConfiguration(), fs));

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ComponentBundler.BundleLibs();
            ComponentBundler.BundleComponent("Test", fs);
        }
    }
}
