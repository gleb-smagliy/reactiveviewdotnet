var nuspecFile =  Argument<string>("artifactsDir", @"..\artifacts") + @"\ReactiveViewDotNet.nuspec";
var packageDir = "../package";

Task("Pack")
.Does(()=> 
{
	NuGetPack(new FilePath(nuspecFile), new NuGetPackSettings());
	EnsureDirectoryExists(packageDir);
	CleanDirectory(packageDir);
	MoveFiles("./*.nupkg", packageDir);
});

RunTarget("Pack");