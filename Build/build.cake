#tool "nuget:?package=NUnit.ConsoleRunner&version=3.6.0"
#addin "Cake.Npm"

var configuration = Argument("configuration", "Debug");
var stepsArg = Argument<string>("steps", "");
var artifactsDir =  Argument<string>("artifactsDir", @"..\artifacts");

Information("Steps: {0}", stepsArg);
var steps = stepsArg.Split(',');

if(steps.Length == 1 && string.IsNullOrEmpty(steps[0]))
{
	steps = new string[0];
}

Information("Configuration: " + configuration);

var MainProjectPath = @"..\ReactiveViewDotNet\ReactiveViewDotNet\bin\" + configuration;
var UnitTestsPath = @"..\ReactiveViewDotNet\ReactiveViewDotNet.UnitTests\bin\" + configuration;
var IntegrationTestsPath = @"..\ReactiveViewDotNet\ReactiveViewDotNet.IntegrationTests\bin\" + configuration;
var ScriptsPath = @"..\ReactiveViewDotNet\ReactiveViewDotNet\Scripts";

var scriptsCopyToDirectories = new []
{
	MainProjectPath,
	IntegrationTestsPath
};

Task("Build")
	.WithCriteria(() => (steps.Contains("Build") || steps.Length == 0))
	.Does(() =>
{
	NuGetRestore("../ReactiveViewDotNet/ReactiveViewDotNet.sln");
	DotNetBuild("../ReactiveViewDotNet/ReactiveViewDotNet.sln", x => x
        .SetConfiguration(configuration)
        .SetVerbosity(Verbosity.Minimal)
        .WithTarget("build")
        .WithProperty("TreatWarningsAsErrors", "false")
    );
});

Task("Tests::Unit")
.WithCriteria(() => (steps.Contains("Tests::Unit") || steps.Length == 0))
.IsDependentOn("Build")
.Does(()=> 
{
	var path = UnitTestsPath + @"\ReactiveViewDotNet.UnitTests.dll";

	Information(path);

	NUnit3(new FilePath[] { UnitTestsPath + @"\ReactiveViewDotNet.UnitTests.dll" });
});

Task("Scripts::InstallPackages")
.WithCriteria(() => (steps.Contains("Scripts::InstallPackages") || steps.Length == 0))
.IsDependentOn("Tests::Unit")
.Does(()=> 
{
		Npm
		.WithLogLevel(NpmLogLevel.Warn)
		.FromPath(ScriptsPath)
		.Install();

		foreach(var dir in scriptsCopyToDirectories)
		{
			CopyFileToDirectory(ScriptsPath + @"\package.json", dir);
			Npm
			.WithLogLevel(NpmLogLevel.Warn)
			.FromPath(dir)			
			.Install();
		}
});

Task("Scripts::Client::Tests")
.IsDependentOn("Scripts::InstallPackages")
.Does(() => 
{
	Npm.FromPath(ScriptsPath)
	.RunScript("client:test");
});

Task("Scripts::Client::Build")
.IsDependentOn("Scripts::Client::Tests")
.Does(() => 
{
	Npm.FromPath(ScriptsPath)
	.RunScript("client:build");

	EnsureDirectoryExists(MainProjectPath + @"\contentFiles\any\any\ClientBundle");
	CopyFiles(ScriptsPath + @"\client\dist\*.js", MainProjectPath + @"\contentFiles\any\any\ClientBundle");
});

Task("Scripts::Server::Tests")
.IsDependentOn("Scripts::InstallPackages")
.Does(() => 
{
	Npm.FromPath(ScriptsPath)
	.RunScript("server:test");
});

Task("Scripts::Server::Build")
.IsDependentOn("Scripts::Server::Tests")
.Does(() => 
{
	Npm.FromPath(ScriptsPath)
	.RunScript("server:build");

	foreach(var dir in scriptsCopyToDirectories)
	{
		CopyFiles(ScriptsPath + @"\server\dist\*.js", dir);
	}
});

Task("Scripts::Build")
.WithCriteria(() => (steps.Contains("Scripts::Build") || steps.Length == 0))
.IsDependentOn("Scripts::Server::Build")
.IsDependentOn("Scripts::Client::Build")
.Does(()=> 
{

});

Task("Tests::Integration")
.WithCriteria(() => (steps.Contains("Tests::Integration") || steps.Length == 0))
.IsDependentOn("Scripts::Build")
.Does(()=> 
{
	CleanDirectory(IntegrationTestsPath + @"\tests_data");
	NUnit3(
		new FilePath[] { IntegrationTestsPath + @"\ReactiveViewDotNet.IntegrationTests.dll" },
		new NUnit3Settings
		{
    		Work = IntegrationTestsPath
    	}
	);
});

Task("CopyArtifacts")
.IsDependentOn("Tests::Integration")
.Does(()=> 
{
	EnsureDirectoryExists(artifactsDir);
	CleanDirectory(artifactsDir);
	
	CopyDirectory(MainProjectPath + @"\contentFiles\any\any\ClientBundle\", artifactsDir + @"\contentFiles\any\any\ClientBundle\");	
	CopyFiles(GetFiles(MainProjectPath + @"\*.pdb"), artifactsDir);
	CopyFiles(GetFiles(MainProjectPath + @"\*.dll"), artifactsDir);
	CopyFiles(GetFiles(MainProjectPath + @"\package.json"), artifactsDir);
	CopyFiles(GetFiles(@"..\Nuspec\ReactiveViewDotNet.targets"), artifactsDir);
	CopyFiles(GetFiles(@"..\Nuspec\ReactiveViewDotNet.nuspec"), artifactsDir);
	// CopyFile(@"..\Nuspec\ReactiveViewDotNet.targets", artifactsDir + @"\ReactiveViewDotNet.targets");
});


RunTarget(steps.LastOrDefault(str => !string.IsNullOrEmpty(str)) ?? "CopyArtifacts");