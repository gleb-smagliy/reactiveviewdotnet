using System.Xml.Linq;

var nuspecFilename = @"\ReactiveViewDotNet.nuspec";

var artifactsDir = Argument<string>("artifactsDir", @"..\artifacts");
var packageDir = Argument<string>("packageDir", @"..\package");
var buildNumber = Argument<string>("buildNumber", "");
var nuspecPath = packageDir + nuspecFilename;

Task("AddBuildNumber")
.Does(()=> 
{
	if(string.IsNullOrEmpty(buildNumber))
	{
		return;
	}

	EnsureDirectoryExists(packageDir);
	CopyDirectory(artifactsDir, packageDir);

	var nuspecElement = LoadNuspec(nuspecPath);
	var newNuspec = AddBuildNumberToVersion(nuspecElement, buildNumber);

	SaveNuspec(nuspecPath, newNuspec);
});

Task("Pack")
.IsDependentOn("AddBuildNumber")
.Does(()=> 
{
	NuGetPack(new FilePath(nuspecPath), new NuGetPackSettings());
	CleanDirectory(packageDir);
	MoveFiles(@".\ReactiveViewDotNet*.nupkg", packageDir);
});

RunTarget("Pack");

private static XElement LoadNuspec(string nuspecPath)
{
	var nuspec = System.IO.File.ReadAllText(nuspecPath);
    return  XElement.Parse(nuspec);
}

private static XElement AddBuildNumberToVersion(XElement nuspecElement, string buildNumber)
{
	XNamespace ns = "http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd";
    var versionElement = nuspecElement.Descendants(ns + "version").First();

    versionElement.Value = versionElement.Value + string.Format("-build{0}", buildNumber);

    return nuspecElement;
}

private static void SaveNuspec(string nuspecPath, XElement nuspecElement)
{
	System.IO.File.WriteAllText(nuspecPath, nuspecElement.ToString(SaveOptions.OmitDuplicateNamespaces), Encoding.UTF8);
}