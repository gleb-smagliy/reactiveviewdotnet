﻿using System;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet.UnitTests
{
    public class ReactComponentTests
    {
        private const string ComponentFolder = @"ComponentFolder\index";

        private IJavascriptEngine _jsEngine;
        private IPhysicalFileSystem _fileSystem;
        private ReactComponent _sut;

        [SetUp]
        public void BeforeTest()
        {
            this._jsEngine = Substitute.For<IJavascriptEngine>();
            this._fileSystem = Substitute.For<IPhysicalFileSystem>();

            InitializeSut();
        }

        private void InitializeSut()
        {
            this._sut = new ReactComponent(this._fileSystem, this._jsEngine, ComponentFolder);
        }

        [Test]
        public void View_ShouldProperlyReturnView_WhenComponentFolderExists()
        {
            this._fileSystem.IsFileExist(Arg.Any<string>()).Returns(true);
            this.InitializeSut();

            var actual = this._sut.View;

            actual.Should().NotBe(null, "because component exists");
        }

        [Test]
        public void View_ShouldThrowInvalidOperationException_WhenComponentFolderDoesNotExist()
        {
            this._fileSystem.IsFileExist(Arg.Any<string>()).Returns(false);
            this.InitializeSut();

            ((Action)(() =>
            {
                var sutView = this._sut.View;
            })).ShouldThrowExactly<InvalidOperationException>("because component folder does not exist");
        }
    }
}