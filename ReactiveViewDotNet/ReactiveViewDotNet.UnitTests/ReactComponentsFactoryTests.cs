﻿using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet.UnitTests
{
    [TestFixture]
    public class ReactComponentsFactoryTests
    {
        private const string AnyComponentViewName = @"AnyComponentViewName\index";

        private IPhysicalFileSystem _physicalFileSystem;
        private ReactComponentsFactory _sut;

        [SetUp]
        public void BeforeTest()
        {
            this._physicalFileSystem = Substitute.For<IPhysicalFileSystem>().EmulateJoin();

            this._sut = new ReactComponentsFactory(this._physicalFileSystem);
        }

        [Test]
        public void CreateComponent_ShouldReturnComponentWithPhysicalPathToFolder_WhenFolderAndMainFileExists()
        {
            this._physicalFileSystem.GetFullUrl(Arg.Is<string>(x => x.Contains(AnyComponentViewName))).Returns(x => x.Arg<string>());
            this._physicalFileSystem.IsFileExist(Arg.Is<string>(x => x.Contains(AnyComponentViewName))).Returns(true);

            var componentCreationResult = this._sut.CreateComponent(AnyComponentViewName);

            componentCreationResult.Component.Should().NotBe(null, "because file {0} should have content and it's url should exist", AnyComponentViewName);
        }

        [Test]
        public void CreateComponent_ShouldReturnNull_WhenComponentFolderDoesNotExist()
        {
            this._physicalFileSystem.IsFileExist(Arg.Is<string>(x => x.Contains(AnyComponentViewName))).Returns(false);

            var componentCreationResult = this._sut.CreateComponent(AnyComponentViewName);

            componentCreationResult.Component.Should().Be(null, "because component {0} should not exist", AnyComponentViewName);
            componentCreationResult.SearchedLocations.Should().HaveCount(1, "because it should search for component in only one folder");
        }

        [Test]
        public void CreateComponent_ShouldReturnNull_WhenComponentUrlDoesNotExist()
        {
            this._physicalFileSystem.GetFullUrl(Arg.Is<string>(x => x.Contains(AnyComponentViewName))).Returns((string)null);
            this._physicalFileSystem.IsFileExist(Arg.Is<string>(x => x.Contains(AnyComponentViewName))).Returns(true);

            var componentCreationResult = this._sut.CreateComponent(AnyComponentViewName);

            componentCreationResult.Component.Should().Be(null, "because {0} should not map to any url for", AnyComponentViewName);
            componentCreationResult.SearchedLocations.Should().HaveCount(1, "because it should search for component in only one folder");
        }

        
        [Test]
        public void CreateComponent_ShouldReturnNull_WhenFolderExistsButMainFileDoesNotExist()
        {
            this._physicalFileSystem.GetFullUrl(Arg.Is<string>(x => x.Contains(AnyComponentViewName))).Returns(x => x.Arg<string>());
            this._physicalFileSystem.IsFileExist(Arg.Is<string>(x => x.Contains(AnyComponentViewName))).Returns(false);

            var componentCreationResult = this._sut.CreateComponent(AnyComponentViewName);

            componentCreationResult.Component.Should().Be(null, "because {0} component main file should not exist", AnyComponentViewName);
            componentCreationResult.SearchedLocations.Should().HaveCount(1, "because it should search for component in only one folder");
        }
    }
}
