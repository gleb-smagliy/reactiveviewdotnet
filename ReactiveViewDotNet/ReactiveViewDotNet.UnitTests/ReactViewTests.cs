﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace ReactiveViewDotNet.UnitTests
{
    [TestFixture]
    public class ReactViewTests
    {
        private ReactView _sut;
        private const string ComponentSource = "export.createComponent = function () { return 'component' }";
        private const string ComponentPhysicalUrl = "component/url";
        private IJavascriptEngine _jsEngine;
        private ViewContext _viewContext;
        private TextWriter _writer;
        private const string DomContainerIdValue = "IdValue";
        public const string ResultString = "ResultString";

        [SetUp]
        public void BeforeTest()
        {
            this._jsEngine = Substitute.For<IJavascriptEngine>();
            
            this._viewContext = new ViewContext
            {
                ViewData = new ViewDataDictionary
                {
                    Model = new ReactComponentModel
                    {
                        ContainerId = DomContainerIdValue
                    }
                }
            };

            this._writer = Substitute.For<TextWriter>();
        }

        private void CreateSutWithTaskResult(object taskResult)
        {
            this._jsEngine.Compile(Arg.Any<string>()).Returns(input => Task.FromResult(taskResult));
            this._jsEngine.ClearReceivedCalls();
            this.InitializeSut();
        }

        private void InitializeSut()
        {
            this._sut = new ReactView(this._jsEngine, ComponentSource, ComponentPhysicalUrl);
        }

        private string CallRenderOnSut()
        {
            string result = null;

            this._writer.When(x => x.Write(Arg.Any<string>())).Do(Callback.Always(x => result = x.Arg<string>()));

            this._sut.Render(this._viewContext, this._writer);

            return result;
        }

        [Test]
        public void Render_ShouldWriteHtmlFromJavascriptComponent_WhenReactViewJsReturnsString()
        {
            this.CreateSutWithTaskResult(ResultString);

            var actual = this.CallRenderOnSut();

            var expectedRegexp = string.Format(@"<div\s+?id="".*?""\s*?>{0}</div>.*?", ResultString);

            actual.Should().MatchRegex(expectedRegexp, "because component should be wrapped with div container that has id with ComponentId");
        }

        [Test]
        public void Render_ShouldThrowRenderingException_WhenReactViewRenderingThrowsExceptionInJs()
        {
            this._jsEngine.Compile(Arg.Any<string>()).Returns((input) => Task.Run((Func<object>)(() =>
            {
                throw new Exception();
            })));
            this._jsEngine.ClearReceivedCalls();
            this.InitializeSut();

            var action = (Action)(() => this._sut.Render(this._viewContext, this._writer));

            action.ShouldThrow<ReactComponentRenderingException>("because js code throws an exception");
        }

        [Test]
        public void Render_ShouldAddActivationJavascriptCodeWithProperId_WhenReactViewIsRenderedInJs()
        {
            this.CreateSutWithTaskResult(ResultString);

            var actual = this.CallRenderOnSut();

            var expectedRegexp = string.Format(@".+?<script type=""text/javascript"">.*?{0}.*?</script>", ComponentPhysicalUrl);

            Regex.Match(actual, expectedRegexp, RegexOptions.Singleline).Success.Should()
                .Be(true, "because component {0} should be followed by activation code that has component physical path inside and match following regexp: \"{1}\"", actual, expectedRegexp);
        }
        
        [Test]
        public void Render_ShouldWrapJsComponentWithContainer_OnAnyComponent()
        {
            this.CreateSutWithTaskResult(ResultString);

            var actual = this.CallRenderOnSut();

            var expectedRegexp = string.Format(@"<div\s+?id="".*?{0}.*?""\s*?>{1}</div>.*?", DomContainerIdValue, ResultString);

            actual.Should().MatchRegex(expectedRegexp, "because component should be wrapped with div container that has id with ComponentId");
        }
        
        [Test]
        public void Render_ShouldRenderSpecifiedReactJsComponent_WhenComponentSpecifiedInConfiguration()
        {
            this.CreateSutWithTaskResult(ResultString);

            this.CallRenderOnSut();

            this._jsEngine
                .ReceivedCalls()
                .Should()
                .HaveCount(1, "because compile should be called once inside constructor of ReactView")
                .And
                .Contain(call => ((string)call.GetArguments()[0]).Contains(ComponentSource), "because view should use specified component to render it");
        }
    }
}