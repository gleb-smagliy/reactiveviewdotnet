﻿using NSubstitute;
using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet.UnitTests
{
    internal static class FileSystemHelper
    {
        public static IPhysicalFileSystem EmulateJoin(this IPhysicalFileSystem fileSystem, string separator = "\\")
        {
            fileSystem.JoinPaths(Arg.Any<string[]>()).Returns(call => string.Join("\\", call.Arg<string[]>()));

            return fileSystem;
        }

        public static IPhysicalFileSystem EmulateFullUrl(this IPhysicalFileSystem fileSystem, string path, string url)
        {
            fileSystem.GetFullUrl(path).Returns(url);

            return fileSystem;
        }
    }
}