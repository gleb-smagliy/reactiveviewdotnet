﻿using System;
using System.IO;
using FluentAssertions;
using NUnit.Framework;
using ReactiveViewDotNet.ComponentBundling;

namespace ReactiveViewDotNet.IntegrationTests
{
    [TestFixture]
    public class ModulesBundlerTests
    {
        private const string ComponentDirectory = @"tests_data\components_source\test_component";

        private ModulesBundler _sut;

        [SetUp]
        public void BeforeTest()
        {
            this._sut = new ModulesBundler();
        }

        [Test]
        [TestCase(@"react", @"tests_data\modules_bundling_output", @"tests_data\modules_bundling_output\react.js")]
        public void BundleModules_ShouldBundleSpecifiedModulesToTheSpecifiedOutputDir(string moduleName, string outputDir, string expectedOutputModule)
        {
            var settings = new ModulesBundlingSettings
            {
                ComponentsNames = new[]
                {
                 moduleName
                },
                OutputDirectory = ConvertPathToAbsolute(outputDir),
                ModulesDirectory = ConvertPathToAbsolute("node_modules")
            };

            this._sut.Bundle(settings);

//            Console.WriteLine("moduleName: {0}", moduleName);
//            Console.WriteLine("outputDir: {0}", moduleoutputDirName);
//            Console.WriteLine("moduleName: {0}", moduleName);
            File.ReadAllBytes(ConvertPathToAbsolute(expectedOutputModule)).Length.Should().BeGreaterThan(0);
        }

        private static string ConvertPathToAbsolute(params string[] paths)
        {
            return Path.Combine(FileSystemHelper.AssemblyDirectory, Path.Combine(paths));
        }
    }
}