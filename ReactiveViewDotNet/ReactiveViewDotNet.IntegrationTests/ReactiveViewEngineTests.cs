﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using FluentAssertions;
using NUnit.Framework;
using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet.IntegrationTests
{
    [TestFixture]
    public class ReactiveViewEngineTests
    {
        private ReactiveViewEngine _sut;
        private IPhysicalFileSystem _fileSystem;
        private ReactiveViewEngineConfiguration _configuration;

        private const string ExistingViewName = @"views\existing_component\index";
        private const string NotExistingViewName = @"views\not_existing_component\index";

        [SetUp]
        public void BeforeTest()
        {
            this._fileSystem = new PhysicalFileSystem(FileSystemHelper.AssemblyDirectory);
            this._configuration = new ReactiveViewEngineConfiguration();

            this._sut = new ReactiveViewEngine(this._configuration, this._fileSystem);
        }

        [Test]
        public void FindView_ShouldReturnValidViewResult_WhenComponentExists()
        {
            var viewResult = this._sut.FindView(new ControllerContext(), ExistingViewName, null, false);

            viewResult.View.Should().NotBeNull();
        }

        [Test]
        public void FindView_ShouldReturnViewResultWithProperSearchedLocations_WhenComponentDoesNotExists()
        {
            var viewResult = this._sut.FindView(new ControllerContext(), NotExistingViewName, null, false);

            viewResult.View.Should().BeNull();
            viewResult.SearchedLocations.Should().HaveCount(1);
            viewResult.SearchedLocations.First().Should().Be(FileName(NotExistingViewName));
        }

        [Test]
        public void FindPartialView_ShouldReturnValidViewResult_WhenComponentExists()
        {
            var viewResult = this._sut.FindView(new ControllerContext(), ExistingViewName, null, false);

            viewResult.View.Should().NotBeNull();
        }

        [Test]
        public void FindPartialView_ShouldReturnViewResultWithProperSearchedLocations_WhenComponentDoesNotExists()
        {
            var viewResult = this._sut.FindView(new ControllerContext(), NotExistingViewName, null, false);

            viewResult.View.Should().BeNull();
            viewResult.SearchedLocations.Should().HaveCount(1);
            viewResult.SearchedLocations.First().Should().Be(FileName(NotExistingViewName));
        }

        private static string FileName(string viewName)
        {
            return viewName + ".js";
        }
    }
}