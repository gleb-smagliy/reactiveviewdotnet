﻿using System.IO;
using System.Web.Mvc;
using FluentAssertions;
using NUnit.Framework;
using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet.IntegrationTests
{
    [TestFixture]
    public class ReactComponentIntegrationTests
    {
        private const string ComponentString = "renderable_component_test.rjs";

        private ReactComponent _sut;
        private ViewContext _viewContext;

        [SetUp]
        public void BeforeTest()
        {
            this._sut = new ReactComponent(new PhysicalFileSystem(FileSystemHelper.AssemblyDirectory, "/"),
                new EdgeJavascriptEngine(), "views/renderable_component.rjs/index.js");

            this._viewContext = new ViewContext
            {
                ViewData = new ViewDataDictionary
                {
                    Model = new ReactComponentModel
                    {
                        ContainerId = "IdComponentTest"
                    }
                }
            };
        }

        [Test]
        public void ReactComponentViewRender_ShouldRenderComponentToHtml()
        {
            var writer = new StringWriter();

            this._sut.View.Render(this._viewContext, writer);

            writer.ToString().Should().Contain(ComponentString, "because component should be rendered as html");
        }
    }
}