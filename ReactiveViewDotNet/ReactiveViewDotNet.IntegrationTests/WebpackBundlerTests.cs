﻿using System;
using System.IO;
using FluentAssertions;
using NUnit.Framework;
using ReactiveViewDotNet.ComponentBundling;

namespace ReactiveViewDotNet.IntegrationTests
{
    [TestFixture]
    public class WebpackBundlerTests
    {
        private const string NodeModulesDirectory = "node_modules";

        private const string ComponentDirectory = @"tests_data\components_source\test_component";
        private const string ComponentFilename = @"index";
        private const string ServerOutputDirectory = @"tests_data\components_output\server_output";
        private const string ClientOutputDirectory = @"tests_data\components_output\client_output";
        private const string OutputFilename = @"index-output";

        private const string ComponentSourceCode = "console.log('I am test component');";

        private const string AfterComponentSourceCode = "window.ReactiveViews.setComponent('superComponent', module.exports);";

        private static readonly string ModulesDirectory = ConvertPathToAbsolute(NodeModulesDirectory);

        private WebpackBundler _sut;
        private BundlingSettings _settings;

        [SetUp]
        public void BeforeTest()
        {
            this._sut = new WebpackBundler();

            this._settings = new BundlingSettings
            {
                ComponentDirectory = ConvertPathToAbsolute(ComponentDirectory),
                EntryFilename = ComponentFilename,
                ServerOutputDirectory = ConvertPathToAbsolute(ServerOutputDirectory),
                ClientOutputDirectory = ConvertPathToAbsolute(ClientOutputDirectory),
                OutputFilename = OutputFilename,
                ModulesDirectory = ModulesDirectory,
                AfterComponentSourceCode = AfterComponentSourceCode,
                ExternalModules = new string[0]
            };
        }

        [Test]
        public void Bundle_ShouldReturnTrue_WhenBundlingIsSuccessfull()
        {
            ComponentMockingHelper.WriteComponentSource(ConvertPathToAbsolute(ComponentDirectory, ComponentFilename), ComponentSourceCode);

            var actual = this._sut.Bundle(this._settings);

            actual.Should().BeTrue("because bundling should be successful");
        }

        [Test]
        public void Bundle_ShouldThrowBundlingException_WhenSettingsAreInvalid()
        {
            ((Action)(() => this._sut.Bundle(new BundlingSettings()))).ShouldThrow<WebpackComponentBundlingException>("because empty settings is not valid");
        }

        [Test]
        [TestCase(ServerOutputDirectory, Description = "Server component bundle")]
        [TestCase(ClientOutputDirectory, Description = "Client component bundle")]
        public void Bundle_ShouldCreateSpecifiedAsOutputFileInTheOutputDir_WhenOutputIsProperlySpecified(string outputDirectory)
        {
            ComponentMockingHelper.WriteComponentSource(ConvertPathToAbsolute(ComponentDirectory, ComponentFilename), ComponentSourceCode);

            this._sut.Bundle(this._settings);

            File.Exists(ConvertPathToAbsolute(ServerOutputDirectory, OutputFilename))
                .Should().BeTrue("because file should be bundled to the output directory");
        }

        private static string ConvertPathToAbsolute(params string[] paths)
        {
            return Path.Combine(FileSystemHelper.AssemblyDirectory, Path.Combine(paths));
        }
    }
}