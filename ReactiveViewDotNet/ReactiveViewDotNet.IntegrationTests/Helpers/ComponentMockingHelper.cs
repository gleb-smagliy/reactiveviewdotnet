﻿using System.IO;

namespace ReactiveViewDotNet.IntegrationTests
{
    public class ComponentMockingHelper
    {
        public static void WriteComponentSource(string path, string componentSource)
        {
            var directory = Path.GetDirectoryName(path);

            if (!string.IsNullOrEmpty(directory) && !Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            File.WriteAllText(path, componentSource);
        }
    }
}