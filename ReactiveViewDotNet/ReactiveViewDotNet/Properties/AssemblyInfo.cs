﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("ReactiveViewDotNet")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("ReactiveViewDotNet")]
[assembly: AssemblyCopyright("Copyright ©  Gleb Smagliy 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("2bf76c3c-8a28-4e2b-a8f1-72a0df06ae28")]
[assembly: AssemblyVersion("0.1.1.0")]
[assembly: AssemblyFileVersion("0.1.1.0")]
[assembly: AssemblyInformationalVersion("0.1.1.0")]
[assembly: InternalsVisibleTo("ReactiveViewDotNet.UnitTests")]
[assembly: InternalsVisibleTo("ReactiveViewDotNet.IntegrationTests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
