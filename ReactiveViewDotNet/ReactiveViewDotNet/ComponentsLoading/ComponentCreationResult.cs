﻿namespace ReactiveViewDotNet
{
    internal class ComponentCreationResult
    {
        public string[] SearchedLocations { get; set; }

        public IReactComponent Component { get; set; }
    }
}