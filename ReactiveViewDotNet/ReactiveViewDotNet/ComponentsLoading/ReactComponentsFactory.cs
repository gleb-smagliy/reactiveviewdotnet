﻿using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet
{
    internal class ReactComponentsFactory:IReactComponentsFactory
    {
        private const string ViewFileExtension = ".js";

        private readonly IPhysicalFileSystem _fileSystem;

        public ReactComponentsFactory(IPhysicalFileSystem fileSystem)
        {
            this._fileSystem = fileSystem;
        }

        public ComponentCreationResult CreateComponent(string viewName)
        {
            var componentFilename = CreateComponentFilename(viewName);

            var component = CreateReactComponent(componentFilename);

            if (component != null)
            {
                return new ComponentCreationResult
                {
                    Component = component
                };
            }

            return new ComponentCreationResult
            {
                Component = null,
                SearchedLocations = new[] { componentFilename }
            };
        }

        private static string CreateComponentFilename(string viewName)
        {
            return viewName + ViewFileExtension;
        }

        private IReactComponent CreateReactComponent(string componentFolder)
        {
            var jsEngine = new EdgeJavascriptEngine();

            var component = new ReactComponent(this._fileSystem, jsEngine, componentFolder);
            
            if (component.IsValid)
            {
                return component;
            }

            return null;
        }
    }
}