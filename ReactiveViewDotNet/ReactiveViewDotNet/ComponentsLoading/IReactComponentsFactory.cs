﻿namespace ReactiveViewDotNet
{
    internal interface IReactComponentsFactory
    {
        ComponentCreationResult CreateComponent(string viewName);
    }
}