var webpack = require("webpack");
var path = require("path");
var fs = require("fs");

function getEntry(context)
{
	//console.log(path.join(context, '\\package.json'));

	const modulePath = path.join(context, 'package.json');

	if(!fs.existsSync(modulePath))
	{
		console.log('modulePath: ' + modulePath);

		return {
			success:false,
			message: "file does not exists"
		};
	}

	return {
		success: true,
		result: JSON.parse(fs.readFileSync(modulePath, {encoding: "utf-8"})).main
	};
}

function bundleSingleModule(moduleName, modulesDirectory, externals, outputDirectory, loadersDirectory)
{
	const moduleDirectory = path.join(modulesDirectory, moduleName),
		  outputFilename = moduleName + '.js';

	const entryFilenameResult = getEntry(moduleDirectory);

	if(entryFilenameResult.success === false)
	{
		return new Promise((resovle, reject) => reject(new Error(entryFilenameResult.message)));
	}

	const entryFilename = entryFilenameResult.result;


	return new Promise((resolve, reject) =>
	{
		webpack(
			{
				context: path.resolve(moduleDirectory),
				entry: ['./', entryFilename].join(''),
				externals:externals,
				output:
				{
					path: path.resolve(outputDirectory),
					filename: outputFilename
				},
				 resolveLoader:
				{
				  modules:
				  [
					  path.resolve(modulesDirectory)
				  ]
				},
				module:
					{
						rules:
						[
							{
								test: path.resolve(path.join(moduleDirectory, entryFilename)),
								use:
								[
									{
										loader: 'expose-loader',
										options: moduleName
									}
								]
							}
						]
					}
			},
			function(err, stats)
			{
				if (err) { throw err; }

				var jsonStats = stats.toJson();

				if(jsonStats.errors.length > 0)
				{
					reject(new Error(JSON.stringify(jsonStats.errors, null, 2)), false);
					return;
				}

				resolve(path.join(outputDirectory, outputFilename));
			}
		);
	});
}

function bundle(settings, callback)
{
		const componentsNames = settings.ComponentsNames,
			  modulesDirectory = settings.ModulesDirectory,
			  outputDirectory = settings.OutputDirectory,
			  bundlingPromises = componentsNames
				.map((name) => bundleSingleModule(name, modulesDirectory, componentsNames , outputDirectory));

		Promise.all(bundlingPromises)
			   .then(
			   		(pathes) => { callback(null, pathes); },
			   		(err) => { callback(err, null); }
		   		);
}

export { bundle }
