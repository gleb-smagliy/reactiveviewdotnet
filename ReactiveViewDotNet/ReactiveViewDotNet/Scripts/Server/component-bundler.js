var webpack = require("webpack");
var nodeExternals = require("webpack-node-externals");
var path = require("path");
var requireResolve = require('require-resolve');

function createCommonTarget(settings, target, modulesExternals, outputDirectory)
{
	const componentDirectory = settings.ComponentDirectory,
	    entryFilename = settings.EntryFilename,
	    outputFilename = settings.OutputFilename,
	    modulesDirectory = settings.ModulesDirectory;

	return {
		context: path.resolve(componentDirectory),
	    entry: "./" + entryFilename,
	    target:target,
	    externals:modulesExternals,
        resolve:
        {
            modules:
            [
                path.resolve(modulesDirectory)
            ]
        },
        resolveLoader:
	    {
		  modules:
		  [
		    modulesDirectory
		  ]
		},
	    output:
	    {
	        path: path.resolve(outputDirectory),
	        filename: outputFilename,
	    },
        module:
	    {
		    loaders:
		    [
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    use:
                        [
                            {
                                loader: 'babel-loader',
                                options:
                                {
                                    "presets":
                                    [
                                        "es2015",
                                        "stage-0",
                                        "react"
                                    ].map(p => path.join(modulesDirectory, "babel-preset-" + p))
                                }
                            }
                        ]
                }
		    ]
	    }
	};
}

function addLibraryOutput(configuration)
{
	configuration.output.library = '';
	configuration.output.libraryTarget = 'commonjs';

	return configuration;
}

function addWrapLoader(previousConfiguration, settings)
{
	const sourceCode = settings.AfterComponentSourceCode,
		entryFilename = settings.EntryFilename,
		componentDirectory = settings.ComponentDirectory;


	previousConfiguration.module.loaders.push(
	{
		loader: "wrap-module-loader",
		test: (pathToModule) =>
		{
			const resolvedEntryPath = path.resolve(path.join(componentDirectory, entryFilename).replace(/\\/g, '/'));
			const resolvedPathToModule = path.resolve(pathToModule);

			return resolvedEntryPath  === resolvedPathToModule;
		},
		query:
		{
			after: sourceCode
		}
	});

	return previousConfiguration;
}

function bundle(bundlingSettingsDto, callback)
{
		const modulesExternals = bundlingSettingsDto.ExternalModules,
		      serverOutputDirectory = bundlingSettingsDto.ServerOutputDirectory,
		      clientOutputDirectory = bundlingSettingsDto.ClientOutputDirectory,
			  modulesDirectory = bundlingSettingsDto.ModulesDirectory;

		//TODO: make each configuration immutable

		webpack(
			[
				addLibraryOutput(createCommonTarget(bundlingSettingsDto, "node", nodeExternals({modulesDir: modulesDirectory}), serverOutputDirectory)),
				addWrapLoader(createCommonTarget(bundlingSettingsDto, "web", modulesExternals, clientOutputDirectory), bundlingSettingsDto)
			],
			function(err, stats)
			{
		    	if (err) { throw err; }

				var jsonStats = stats.toJson();

				if(jsonStats.errors.length > 0)
				{
					callback(new Error(JSON.stringify(jsonStats.errors, null, 2)), false);
					return;				
				}

				callback(null, true);
			});
}

export { bundle }
