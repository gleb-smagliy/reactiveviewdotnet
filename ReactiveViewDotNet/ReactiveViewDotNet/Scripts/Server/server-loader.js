import { renderToString } from 'react-dom/server';
import { Script } from 'vm';
import { wrap as wrapSource } from 'module';

const load =  (componentSource, outerRequire) =>
	{
		const componentModule =
		{
			exports:{}
		};

		new Script(wrapSource(componentSource)).runInNewContext({global:{}})(componentModule.exports, outerRequire, componentModule, __filename, __dirname);

		return {"render": (initialState) =>
		{		
			const component = componentModule.exports.createComponent(initialState);

			return renderToString(component);
		}};
	};


export { load };


