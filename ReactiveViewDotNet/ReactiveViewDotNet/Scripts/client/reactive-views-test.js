var assert = require('assert');
var sinon = require('sinon');
var mockery = require('mockery');

describe('ReactiveViews', function()
{
    var scriptSetAttributeStub,
        reactiveViews,
        renderSpy;

    const componentLink = 'component/link/index.js',
        componentContainerId = 'componentId',
        initialState = {};

    before(function ()
    {
        mockery.enable({
            warnOnReplace: false,
            warnOnUnregistered: false
        });
    });

    after(function ()
    {
        mockery.disable();
    });

    beforeEach(function ()
    {
        renderSpy = sinon.spy();
        scriptSetAttributeStub = sinon.stub();
        reactiveViews = require('./reactive-views');

        mockery.registerMock('react-dom', { render:renderSpy });

        global.window = {};
        global.document =
        {
            createElement: sinon.stub().returns({ setAttribute: scriptSetAttributeStub }),
            getElementById: sinon.stub().returns(null),
            head: { appendChild: sinon.stub() }
        };
    });

    describe('setComponent', function()
    {
        it('should resolve promise setted with renderComponent in the past', function()
        {
            reactiveViews.renderComponent(componentLink, componentContainerId, initialState);
            reactiveViews.setComponent(componentLink, {createComponent:sinon.stub().returns(null)});

            renderSpy.calledWith(null, null);
        });

        it('should create promise and resolve it if no renderComponent was called before', function()
        {
            reactiveViews.setComponent(componentLink, {createComponent:sinon.stub().returns(null)});
            reactiveViews.renderComponent(componentLink, componentContainerId, initialState);

            renderSpy.calledWith(null, null);
        });
    });

    describe('renderComponent', function()
    {
        it('should add script tag to the page with componentLink if this script tag was not added', function()
        {
            reactiveViews.renderComponent(componentLink, componentContainerId, initialState);

            scriptSetAttributeStub.calledWith('src', componentLink);
        });

        it('should not add script tag to the page with componentLink if this script tag was already added by the render', function()
        {
            reactiveViews.renderComponent(componentLink, componentContainerId, initialState);
            reactiveViews.renderComponent(componentLink, componentContainerId, initialState);

            global.document.createElement.calledOnce;
        });
    });
});