var nodeExternals = require('webpack-node-externals');
var webpackConfig = require('./webpack.config');

module.exports = {
    target: 'node',
    module: webpackConfig.module,
    externals: [nodeExternals()]
};