var bundler = require('./modules-bundler');
var bundlerTestsHelper = require('./helpers/bundlers-tests-helper');
var path = require('path');
var fs = require('fs');
import { Script } from 'vm';
import { assert } from 'chai';

describe('ModulesBundler', function()
{
    describe('bundle', function()
    {
        var commonBundlingSettings,
            testDirectoryContext,
            testDirectoryContextFactory,
            outputDirectory;

        beforeEach(function()
        {
            testDirectoryContext = bundlerTestsHelper.createDirectoryContext();
            testDirectoryContextFactory = (pathToJoin) => path.join(testDirectoryContext, pathToJoin);

            outputDirectory = testDirectoryContextFactory('Output');

            commonBundlingSettings =
            {
                ComponentsNames: [],
                OutputDirectory: outputDirectory,
                ModulesDirectory: path.resolve('node_modules')
            };
        });

        afterEach(() =>
        {
            bundlerTestsHelper.removeDirectoryContext();
        });

        it('should create one server file for only one module according to settings', (done) =>
        {
            commonBundlingSettings.ComponentsNames = ['react'];

            bundler.bundle(commonBundlingSettings, (err, result) =>
            {
                assert.isTrue(fs.existsSync(path.join(outputDirectory, 'react.js')));

                done();
            });
        });

        it('should create two server files for two modules according to settings', function(done)
        {
            commonBundlingSettings.ComponentsNames = ['react', 'fbjs'];

            bundler.bundle(commonBundlingSettings, (err, result) =>
            {
                const modulesPathes = commonBundlingSettings.ComponentsNames.map(x=>path.join(outputDirectory, x + '.js'));

                assert.isTrue(fs.existsSync(path.join(outputDirectory, 'react.js')));
                assert.isTrue(fs.existsSync(path.join(outputDirectory, 'fbjs.js')));

                done();
            });
        });

        it('should call callback with pathes of bundled modules if bundling was successfull', function(done)
        {
            commonBundlingSettings.ComponentsNames = ['react', 'fbjs'];

            bundler.bundle(commonBundlingSettings, (err, result) =>
            {
                assert.deepEqual(result, [path.join(outputDirectory, 'react.js'), path.join(outputDirectory, 'fbjs.js')]);

                done();
            });
        });

        it('should call callback with exception if module was specified incorrectly', function(done)
        {
            commonBundlingSettings.ComponentsNames = ['33333333react'];

            bundler.bundle(commonBundlingSettings, (err, result) =>
            {
                assert.isNotNull(err);

                done();
            });
        });

        it('should create bundles exposing modules by their names', function(done)
        {
            commonBundlingSettings.ComponentsNames = ['react'];

            bundler.bundle(commonBundlingSettings, (err, result) =>
            {
                var sandbox = {};
                const bundledComponentFile = path.join(outputDirectory, 'react.js');
                const componentScript = (new Script(fs.readFileSync(bundledComponentFile, {encoding: "utf-8"})))
                    .runInNewContext(sandbox);

                assert.property(sandbox, 'react');

                done();
            });
        });

        it('should pass all the specified modules as externals for bundling modules');
    });
});