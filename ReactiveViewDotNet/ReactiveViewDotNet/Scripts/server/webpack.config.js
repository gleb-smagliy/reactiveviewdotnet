const nodeExternals = require('webpack-node-externals');

module.exports = {
		context: __dirname,
		entry:
		{
			'component-bundler': './component-bundler.js',
			'modules-bundler': './modules-bundler.js',
			'server-loader': './server-loader.js'
		},
		output:
		{
			path: __dirname + '/dist',
			filename: '[name].js',
			library: '',
			libraryTarget: 'commonjs'
		},
		target: 'node',
		externals: nodeExternals(),
        module:
		{
			rules: 
			[
				{
					test: /\.jsx?$/,
					exclude: /node_modules/,
					use:
					[
						{
				  			loader: 'babel-loader',
				  			options:
				  			{
			  				    "presets":
			  				    [
						      		[
						        		"es2015",
						        		{
						          			"loose": true
						        		}
						      		],
						      		"stage-0",
						      		"react"
					    		]
				  			}
						}
					]
				}
			]
		}
	};