import { writeToFile, createDirectoryContext, removeDirectoryContext} from './helpers/bundlers-tests-helper';
import { load } from './server-loader';
import { assert } from 'chai';
import * as path from 'path';

describe('ServerLoader', function()
{
    describe('load', function()
    {
        var testDirectoryContext,
            testDirectoryContextFactory;

        const componentFileName = 'test-component.js',
            sourceCode = 'require("react");';

        beforeEach(function()
        {
            testDirectoryContext = createDirectoryContext();
            testDirectoryContextFactory = (pathToJoin) => path.join(testDirectoryContext, pathToJoin);
        });

        afterEach(function()
        {
            removeDirectoryContext();
        });

        it('should return object with render function');

        it('should pass require function to the executing component', () =>
        {
            assert.isFunction(load(sourceCode, eval('require')).render);
        });

        it('should pass exports object to the executing component');
    });
});