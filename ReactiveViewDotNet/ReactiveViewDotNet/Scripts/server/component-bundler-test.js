var bundler = require('./component-bundler');
var bundlerTestsHelper = require('./helpers/bundlers-tests-helper');
var path = require('path');
var fs = require('fs');
import { assert } from 'chai';
import { Script } from 'vm';

describe('ComponentBundler', function()
{
    var commonBundlingSettings,
        testDirectoryContext,
        testDirectoryContextFactory,
        componentDirectory,
        serverOutputDirectory,
        clientOutputDirectory;

    const componentFileName = 'index.js',
          sourceCode = 'test.componentExecuted=true;';

    beforeEach(function()
    {
        testDirectoryContext = bundlerTestsHelper.createDirectoryContext();
        testDirectoryContextFactory = (pathToJoin) => path.join(testDirectoryContext, pathToJoin);

        componentDirectory = testDirectoryContextFactory('componentDirectory');
        serverOutputDirectory = testDirectoryContextFactory('Server');
        clientOutputDirectory = testDirectoryContextFactory('Client');

        writeToFile(path.join(componentDirectory, componentFileName), sourceCode);
        commonBundlingSettings =
        {
            ComponentDirectory: componentDirectory,
            EntryFilename: componentFileName,
            OutputFilename: componentFileName,
            ModulesDirectory: path.resolve('node_modules'),
            ServerOutputDirectory: serverOutputDirectory,
            ClientOutputDirectory: clientOutputDirectory,
            AfterComponentSourceCode: 'test.afterComponentSourceCodeExecuted = true;'
        };
    });

    afterEach(function()
    {
        bundlerTestsHelper.removeDirectoryContext();
    });

    function writeToFile(fileName, source)
    {
        if(!fs.existsSync(path.dirname(fileName)))
        {
            fs.mkdirSync(path.dirname(fileName));
        }

        fs.writeFileSync(fileName, source);
    }

    describe('bundle', function()
    {
        it('should create server file for component according to settings', (done) =>
        {
            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    assert.isTrue(fs.existsSync(path.join(serverOutputDirectory, componentFileName)));

                    done();
                }
            );
        });

        it('should create server file for component according to settings if global entry path is specified', (done) =>
        {
            commonBundlingSettings.ComponentDirectory = path.resolve(commonBundlingSettings.ComponentDirectory);

            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    assert.isTrue(fs.existsSync(path.join(serverOutputDirectory, componentFileName)));

                    done();
                }
            );
        });

        it('should create client file for component according to settings', (done) =>
        {
            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    assert.isTrue(fs.existsSync(path.join(clientOutputDirectory, componentFileName)));

                    done();
                }
            );
        });

        it('should create client file for component according to settings if global entry path is specified', (done) =>
        {
            commonBundlingSettings.ComponentDirectory = path.resolve(commonBundlingSettings.ComponentDirectory);

            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    assert.isTrue(fs.existsSync(path.join(clientOutputDirectory, componentFileName)));

                    done();
                }
            );
        });

        it('should call callback with null error if bundling was successfull', (done) =>
        {
            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    assert.isNull(err);

                    done();
                }
            );
        });

        it('should call callback with "true" result if bundling was successfull', (done) =>
        {
            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    assert.isTrue(result);

                    done();
                }
            );
        });

        it('should call callback with exception if entry file was specified incorrectly', (done) =>
        {
            commonBundlingSettings.EntryFilename = 'NotExistingFile.js';

            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    assert.isNotNull(err);

                    done();
                }
            );
        });

        it('should call callback with false result if entry file was specified incorrectly', (done) =>
        {
            commonBundlingSettings.EntryFilename = 'NotExistingFile.js';

            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    assert.isFalse(result);

                    done();
                }
            );
        });

        it('should bundle client component with wrapper code specified in settings', (done) =>
        {
            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    var sandbox = {test:{}, exports:{}};
                    const bundledComponentFile = path.join(commonBundlingSettings.ClientOutputDirectory, commonBundlingSettings.EntryFilename);
                    const componentScript = (new Script(fs.readFileSync(bundledComponentFile, {encoding: "utf-8"})))
                                            .runInNewContext(sandbox);

                    assert.isTrue(sandbox.test.afterComponentSourceCodeExecuted);

                    done();
                }
            );
        });

        it('should bundle server component without wrapper code specified in settings', (done) =>
        {
            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    var sandbox = {test:{afterComponentSourceCodeExecuted: false}, exports:{}};
                    const bundledComponentFile = path.join(commonBundlingSettings.ServerOutputDirectory, commonBundlingSettings.EntryFilename);
                    const componentScript = (new Script(fs.readFileSync(bundledComponentFile, {encoding: "utf-8"})))
                        .runInNewContext(sandbox);

                    assert.isFalse(sandbox.test.afterComponentSourceCodeExecuted);

                    done();
                }
            );
        });

        it('should use babel loader with es2015 and react presets for client side components', (done) =>
        {
            const sourceCode = `
                                    import { Component } from 'react';

                                    test.Component = () =>
                                    {
                                        return <h1>Hello, test!</h1>;
                                    }`;

            writeToFile(path.join(componentDirectory, componentFileName), sourceCode);

            bundler.bundle(commonBundlingSettings,
                (err, result) =>
                {
                    var sandbox = {test:{}, exports:{}};
                    const bundledComponentFile = path.join(commonBundlingSettings.ClientOutputDirectory, commonBundlingSettings.EntryFilename);
                    const componentScript = (new Script(fs.readFileSync(bundledComponentFile, {encoding: "utf-8"})))
                        .runInNewContext(sandbox);

                    assert.isFunction(sandbox.test.Component);

                    done();
                }
            );
        });
    });
});