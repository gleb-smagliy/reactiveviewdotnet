const fs = require('fs');
const path = require('path');

const contexts = {};
const tempDirectory = '.tmp';

function deleteFolderRecursive (dirPath)
{
    if(fs.existsSync(dirPath))
    {
        fs.readdirSync(dirPath).forEach(function(file, index){
            const curPath = path.join(dirPath, file);
            if(fs.lstatSync(curPath).isDirectory())
            {
                deleteFolderRecursive(curPath);
            }
            else
            {
                fs.unlinkSync(curPath);
            }
        });

        fs.rmdirSync(dirPath);
    }
}

function createTempDirectory(testName)
{
    let currentTestName = getTestName(testName);

    return contexts[currentTestName] = fs.mkdtempSync(path.join(tempDirectory, currentTestName + '.'));;
}

function removeTempDirectory(testName)
{
    const currentTestName = getTestName(testName);
    const currentDir  = contexts[currentTestName];

    if(currentDir)
    {
        deleteFolderRecursive(currentDir);
    }
}

function getTestName(testName)
{
    return testName != undefined ? testName : "current";
}

function writeToFile(fileName, source)
{
    if(!fs.existsSync(path.dirname(fileName)))
    {
        fs.mkdirSync(path.dirname(fileName));
    }

    fs.writeFileSync(fileName, source);
}

module.exports.removeDirectoryContext = removeTempDirectory;
module.exports.createDirectoryContext = createTempDirectory;
module.exports.writeToFile = writeToFile;