(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("webpack");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var webpack = __webpack_require__(1);
var path = __webpack_require__(0);
var fs = __webpack_require__(2);

function getEntry(context) {

	var modulePath = path.join(context, 'package.json');

	if (!fs.existsSync(modulePath)) {
		console.log('modulePath: ' + modulePath);

		return {
			success: false,
			message: "file does not exists"
		};
	}

	return {
		success: true,
		result: JSON.parse(fs.readFileSync(modulePath, { encoding: "utf-8" })).main
	};
}

function bundleSingleModule(moduleName, modulesDirectory, externals, outputDirectory, loadersDirectory) {
	var moduleDirectory = path.join(modulesDirectory, moduleName),
	    outputFilename = moduleName + '.js';

	var entryFilenameResult = getEntry(moduleDirectory);

	if (entryFilenameResult.success === false) {
		return new Promise(function (resovle, reject) {
			return reject(new Error(entryFilenameResult.message));
		});
	}

	var entryFilename = entryFilenameResult.result;

	return new Promise(function (resolve, reject) {
		webpack({
			context: path.resolve(moduleDirectory),
			entry: ['./', entryFilename].join(''),
			externals: externals,
			output: {
				path: path.resolve(outputDirectory),
				filename: outputFilename
			},
			resolveLoader: {
				modules: [path.resolve(modulesDirectory)]
			},
			module: {
				rules: [{
					test: path.resolve(path.join(moduleDirectory, entryFilename)),
					use: [{
						loader: 'expose-loader',
						options: moduleName
					}]
				}]
			}
		}, function (err, stats) {
			if (err) {
				throw err;
			}

			var jsonStats = stats.toJson();

			if (jsonStats.errors.length > 0) {
				reject(new Error(JSON.stringify(jsonStats.errors, null, 2)), false);
				return;
			}

			resolve(path.join(outputDirectory, outputFilename));
		});
	});
}

function bundle(settings, callback) {
	var componentsNames = settings.ComponentsNames,
	    modulesDirectory = settings.ModulesDirectory,
	    outputDirectory = settings.OutputDirectory,
	    bundlingPromises = componentsNames.map(function (name) {
		return bundleSingleModule(name, modulesDirectory, componentsNames, outputDirectory);
	});

	Promise.all(bundlingPromises).then(function (pathes) {
		callback(null, pathes);
	}, function (err) {
		callback(err, null);
	});
}

exports.bundle = bundle;

/***/ })
/******/ ])));