(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("webpack");

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */
/***/ (function(module, exports) {

module.exports = require("require-resolve");

/***/ }),
/* 6 */,
/* 7 */
/***/ (function(module, exports) {

module.exports = require("webpack-node-externals");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var webpack = __webpack_require__(1);
var nodeExternals = __webpack_require__(7);
var path = __webpack_require__(0);
var requireResolve = __webpack_require__(5);

function createCommonTarget(settings, target, modulesExternals, outputDirectory) {
	var componentDirectory = settings.ComponentDirectory,
	    entryFilename = settings.EntryFilename,
	    outputFilename = settings.OutputFilename,
	    modulesDirectory = settings.ModulesDirectory;

	return {
		context: path.resolve(componentDirectory),
		entry: "./" + entryFilename,
		target: target,
		externals: modulesExternals,
		resolve: {
			modules: [path.resolve(modulesDirectory)]
		},
		resolveLoader: {
			modules: [modulesDirectory]
		},
		output: {
			path: path.resolve(outputDirectory),
			filename: outputFilename
		},
		module: {
			loaders: [{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: [{
					loader: 'babel-loader',
					options: {
						"presets": ["es2015", "stage-0", "react"].map(function (p) {
							return path.join(modulesDirectory, "babel-preset-" + p);
						})
					}
				}]
			}]
		}
	};
}

function addLibraryOutput(configuration) {
	configuration.output.library = '';
	configuration.output.libraryTarget = 'commonjs';

	return configuration;
}

function addWrapLoader(previousConfiguration, settings) {
	var sourceCode = settings.AfterComponentSourceCode,
	    entryFilename = settings.EntryFilename,
	    componentDirectory = settings.ComponentDirectory;

	previousConfiguration.module.loaders.push({
		loader: "wrap-module-loader",
		test: function test(pathToModule) {
			var resolvedEntryPath = path.resolve(path.join(componentDirectory, entryFilename).replace(/\\/g, '/'));
			var resolvedPathToModule = path.resolve(pathToModule);

			return resolvedEntryPath === resolvedPathToModule;
		},
		query: {
			after: sourceCode
		}
	});

	return previousConfiguration;
}

function bundle(bundlingSettingsDto, callback) {
	var modulesExternals = bundlingSettingsDto.ExternalModules,
	    serverOutputDirectory = bundlingSettingsDto.ServerOutputDirectory,
	    clientOutputDirectory = bundlingSettingsDto.ClientOutputDirectory,
	    modulesDirectory = bundlingSettingsDto.ModulesDirectory;

	webpack([addLibraryOutput(createCommonTarget(bundlingSettingsDto, "node", nodeExternals({ modulesDir: modulesDirectory }), serverOutputDirectory)), addWrapLoader(createCommonTarget(bundlingSettingsDto, "web", modulesExternals, clientOutputDirectory), bundlingSettingsDto)], function (err, stats) {
		if (err) {
			throw err;
		}

		var jsonStats = stats.toJson();

		if (jsonStats.errors.length > 0) {
			callback(new Error(JSON.stringify(jsonStats.errors, null, 2)), false);
			return;
		}

		callback(null, true);
	});
}

exports.bundle = bundle;

/***/ })
/******/ ])));