import { render } from 'react-dom';

function ReactiveViews()
{
	const components = {};

	this.setComponent = function(componentLink, component)
	{
		let currentPromise = components[componentLink];

		if(!currentPromise)
		{
			currentPromise = components[componentLink] = createPromise();
		}

		currentPromise.resolve(component);
	};

	this.renderComponent = function (componentLink, containerId, initialState)
	{
		var currentPromise = components[componentLink];

		if(!currentPromise)
		{
			addScript(componentLink);

			currentPromise = components[componentLink] = createPromise();
		}

		currentPromise.promise.then(
			(componentModule) =>
			{
				const component = componentModule.createComponent(initialState);

				render(component, document.getElementById(containerId));
			}
		);
	};

	function addScript(componentLink) 
	{
		const scriptTag = document.createElement('script');

		scriptTag.setAttribute('src', componentLink);

		document.head.appendChild(scriptTag);
	}

	function createPromise()
	{
		const result = {};

		result.promise = new Promise((resolve, reject) => { result.resolve = resolve; });

		return result;
	}

	function getComponent (componentLink)
	{
		let component = components[componentLink];

		if(!component)
		{
			component = components[componentLink] = new Promise();
		}

		return component;
	}
}

module.exports = new ReactiveViews();