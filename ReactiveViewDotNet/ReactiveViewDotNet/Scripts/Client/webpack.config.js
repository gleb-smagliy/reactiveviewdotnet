const path = require('path');

module.exports = {
		context: __dirname,
	    entry:
		{
			'reactive-views': './reactive-views.js'
		},
	    output:
	    {
	        path: path.resolve('./dist'),
	        filename: '[name].js'
	    },
        module:
		{
			rules: 
			[
				{
					test: path.resolve('./reactive-views.js'),
					use:
					[
						{
				  			loader: 'expose-loader',
					  		options: 'ReactiveViews'
						}
					]
				},
				{
					test: /\.jsx?$/,
					exclude: /node_modules/,
					use:
					[
						{
				  			loader: 'babel-loader',
				  			options:
				  			{
			  				    "presets":
			  				    [
						      		[
						        		"es2015",
						        		{
						          			"loose": true
						        		}
						      		],
						      		"stage-0",
						      		"react"
					    		]
				  			}
						}
					]
				}
			]
		}
	};