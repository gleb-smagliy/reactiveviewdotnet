﻿using System;
using System.Threading.Tasks;
using EdgeJs;

namespace ReactiveViewDotNet
{
    internal class EdgeJavascriptEngine:IJavascriptEngine
    {
        public Func<object, Task<object>> Compile(string sourceCode)
        {
            return Edge.Func(sourceCode);
        }
    }
}