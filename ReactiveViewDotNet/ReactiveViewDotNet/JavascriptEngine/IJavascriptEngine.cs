﻿using System;
using System.Threading.Tasks;

namespace ReactiveViewDotNet
{
    internal interface IJavascriptEngine
    {
        Func<object, Task<object>> Compile(string sourceCode);
    }
}