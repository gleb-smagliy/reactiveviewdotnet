﻿using System.Web.Mvc;

namespace ReactiveViewDotNet
{
    internal interface IReactComponent
    {
        IView View { get; }
    }
}