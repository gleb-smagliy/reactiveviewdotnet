﻿namespace ReactiveViewDotNet
{
    public class ReactComponentModel
    {
        public string ContainerId { get; set; }
        public object State { get; set; }
    }
}