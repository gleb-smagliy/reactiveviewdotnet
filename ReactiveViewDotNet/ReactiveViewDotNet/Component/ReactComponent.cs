﻿using System;
using System.Web.Mvc;
using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet
{
    internal class ReactComponent:IReactComponent
    {
        private readonly string _javascriptFileUrl;
        private readonly IJavascriptEngine _jsEngine;
        private readonly string _componentSource;

        public bool IsValid { get; private set; }

        public IView View   
        {
            get
            {
                if (!this.IsValid)
                {
                    throw new InvalidOperationException("Cannot get view of invalid component.");
                }

                return new ReactView(this._jsEngine, this._componentSource, this._javascriptFileUrl);
            }
        }

        public ReactComponent(IPhysicalFileSystem fileSystem, IJavascriptEngine jsEngine, string viewName)
        {
            this._componentSource = fileSystem.GetFileContent(viewName);
            this._javascriptFileUrl = fileSystem.GetFullUrl(viewName);
            this._jsEngine = jsEngine;

            this.IsValid = (this._javascriptFileUrl != null && fileSystem.IsFileExist(viewName));
        }
    }
}