﻿using System;

namespace ReactiveViewDotNet
{
    public class ReactComponentRenderingException:Exception
    {
        public ReactComponentRenderingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}