﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace ReactiveViewDotNet
{
    internal class ReactView:IView
    {
        private readonly string _componentUrl;
        private readonly Func<object, Task<object>> _renderingFunc;
        private const string ViewDataStateKey = "state";

        public ReactView(IJavascriptEngine jsEngine, string componentSource, string componentUrl)
        {
            this._componentUrl = componentUrl;
            this._renderingFunc = CreateRenderingFunction(jsEngine, componentSource);
        }

        private static Func<object, Task<object>> CreateRenderingFunction(IJavascriptEngine jsEngine, string componentSource)
        {
            const string jsFormat = @"
                                        var componentRender = require('./../server-loader')
                                                           .load(`{0}`, require)
                                                           .render;

                                        return function(data, callback)
                                        {{
                                            callback(null, componentRender(data));
                                        }};
                                    ";

            var jsCode = string.Format(jsFormat, componentSource);

            return jsEngine.Compile(jsCode);
        }

        public void Render(ViewContext viewContext, TextWriter writer)
        {
            try
            {
                var htmlCodeStringBuilder = new StringBuilder();

                var componentModel = (ReactComponentModel) viewContext.ViewData.Model;

                var initialState = componentModel.State;

                var componentContainerId = "reactContainer-" + componentModel.ContainerId;
                var componentHtml = ((string)(this._renderingFunc(initialState).Result));

                htmlCodeStringBuilder.AppendFormat("<div id=\"{0}\">{1}</div>", componentContainerId, componentHtml);

                object serializedInitialState = JsonConvert.SerializeObject(initialState);

                htmlCodeStringBuilder.AppendFormat(@"
                                                    <script type=""text/javascript"">
                                                        (function()
                                                        {{
                                                            var initialState = {2};
                                                            window.ReactiveViews.renderComponent('{0}','{1}', initialState);
                                                        }})();
                                                    </script>", this._componentUrl, componentContainerId, serializedInitialState);

                writer.Write(htmlCodeStringBuilder.ToString());
            }
            catch (Exception e)
            {
                throw new ReactComponentRenderingException(
                    string.Format("Component located at '{0}' could not be rendered", this._componentUrl), e);
            }
        }
    }
}