﻿using System;
using System.IO;

namespace ReactiveViewDotNet.Filesystems
{
    public class PhysicalFileSystem : IPhysicalFileSystem
    {
        private const char UrlSeparator = '/';

        private readonly string _rootDirPath;
        private readonly string _rootLink;
        private readonly string _rootLinkPhysicalPath;

        public PhysicalFileSystem(string rootDirPath, string rootLink = "/")
            : this(rootDirPath, rootLink, rootDirPath)
        {

        }
        
        public PhysicalFileSystem(string rootDirPath, string rootLink, string rootLinkPhysicalPath)
        {
            this._rootLinkPhysicalPath = rootLinkPhysicalPath;
            this._rootDirPath = rootDirPath;
            this._rootLink = rootLink;
        }

        public string GetFullUrl(string fileName)
        {
            var physicalpath = Path.Combine(this._rootLinkPhysicalPath, fileName);

            if (File.Exists(physicalpath))
            {
                return Uri.EscapeUriString(Path.Combine(this._rootLink, fileName).Replace(Path.DirectorySeparatorChar, UrlSeparator));
            }

            return null;
        }

        public string JoinPaths(params string[] paths)
        {
            return Path.Combine(paths);
        }

        public bool IsFileExist(string fileName)
        {
            return File.Exists(Path.Combine(this._rootDirPath, fileName));
        }

        public string GetFileContent(string fileName)
        {
            var path = Path.Combine(this._rootDirPath, fileName);

            if (File.Exists(path))
            {
                return File.ReadAllText(path);
            }

            return null;
        }

        public static string CombineUrls(string path, string relative)
        {
            if (relative == null)
                relative = string.Empty;

            if (path == null)
                path = string.Empty;

            if (relative.Length == 0 && path.Length == 0)
                return string.Empty;

            if (relative.Length == 0)
                return path;

            if (path.Length == 0)
                return relative;

            path = path.Replace('\\', UrlSeparator);
            relative = relative.Replace('\\', UrlSeparator);

            return path.TrimEnd(UrlSeparator) + UrlSeparator + relative.TrimStart(UrlSeparator);
        }
    }
}
