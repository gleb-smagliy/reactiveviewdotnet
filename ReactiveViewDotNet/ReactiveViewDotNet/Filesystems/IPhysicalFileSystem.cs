﻿namespace ReactiveViewDotNet.Filesystems
{
    public interface IPhysicalFileSystem
    {
        string GetFullUrl(string fileName);
        bool IsFileExist(string fileName);
        string JoinPaths(params string[] paths);
        string GetFileContent(string fileName);
    }
}