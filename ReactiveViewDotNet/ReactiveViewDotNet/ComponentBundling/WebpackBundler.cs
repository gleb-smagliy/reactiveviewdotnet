﻿using System;

namespace ReactiveViewDotNet.ComponentBundling
{
    public class WebpackBundler
    {
        private static readonly IJavascriptEngine Engine = new EdgeJavascriptEngine();

        public bool Bundle(BundlingSettings bundlingSettings)
        {
            var jsFunction = Engine.Compile(
                            @"
                                var bundle = require('./../component-bundler').bundle;

                                return function (data, callback)
                                {
                                    bundle(data, callback);
                                };
                             ");
            try
            {
                return (bool)jsFunction(bundlingSettings).Result;
            }
            catch (Exception e)
            {
                throw new WebpackComponentBundlingException("Error while bundling component", e);
            }
        }
    }
}