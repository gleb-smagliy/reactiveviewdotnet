﻿namespace ReactiveViewDotNet.ComponentBundling
{
    public class BundlingSettings
    {
        public string ComponentDirectory { get; set; }
        public string EntryFilename { get; set; }
        public string ServerOutputDirectory { get; set; }
        public string OutputFilename { get; set; }
        public string ModulesDirectory { get; set; }
        public string AfterComponentSourceCode { get; set; }
        public string ClientOutputDirectory { get; set; }
        public string[] ExternalModules { get; set; }
    }
}