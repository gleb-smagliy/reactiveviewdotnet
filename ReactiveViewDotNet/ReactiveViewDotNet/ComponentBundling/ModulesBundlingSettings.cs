﻿namespace ReactiveViewDotNet.ComponentBundling
{
    public class ModulesBundlingSettings
    {
        public string[] ComponentsNames { get; set; }
        public string OutputDirectory { get; set; }
        public string ModulesDirectory { get; set; }
    }
}