﻿using System;

namespace ReactiveViewDotNet.ComponentBundling
{
    public class WebpackComponentBundlingException : Exception
    {
        public WebpackComponentBundlingException(string message) : base(message)
        {
        }

        public WebpackComponentBundlingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}