﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ReactiveViewDotNet.ComponentBundling
{
    public class ModulesBundler
    {
        private static readonly IJavascriptEngine Engine = new EdgeJavascriptEngine();

        public IEnumerable<string> Bundle(ModulesBundlingSettings bundlingSettings)
        {
            var jsFunction = Engine.Compile(
                @"
                                var bundle = require('./../modules-bundler').bundle;

                                return function (data, callback)
                                {
                                    bundle(data, callback);
                                };
                             ");
            try
            {
                var result = jsFunction(bundlingSettings).Result;

                return ((object[]) result).OfType<string>();
            }
            catch (Exception e)
            {
                throw new WebpackComponentBundlingException("Error while bundling component", e);
            }
        }
    }
}