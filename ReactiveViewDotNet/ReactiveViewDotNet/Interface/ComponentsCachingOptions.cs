﻿namespace ReactiveViewDotNet
{
    public enum ComponentsCachingOptions
    {
        Cached,
        NotCached
    }
}