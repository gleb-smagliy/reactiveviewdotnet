﻿using System;
using System.Web.Mvc;
using ReactiveViewDotNet.Filesystems;

namespace ReactiveViewDotNet
{
    public class ReactiveViewEngine:IViewEngine
    {
        private readonly ReactComponentsFactory _componentsFactory;

        public ReactiveViewEngine(ReactiveViewEngineConfiguration configuration, IPhysicalFileSystem fileSystem)
        {
            switch (configuration.CachingOptions)
            {
                case ComponentsCachingOptions.NotCached:
                    this._componentsFactory = new ReactComponentsFactory(fileSystem);
                    break;
                default:
                    throw new ArgumentException(string.Format("Components caching option {0} is not valid", configuration.CachingOptions));
                    break;
            }
        }

        public ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            return this.CreateViewEngineResult(partialViewName);
        }

        public ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            return this.CreateViewEngineResult(viewName);
        }

        public void ReleaseView(ControllerContext controllerContext, IView view)
        {
            
        }

        private ViewEngineResult CreateViewEngineResult(string viewName)
        {
            var componentCreationResult = this._componentsFactory.CreateComponent(viewName);

            if (componentCreationResult.Component != null)
            {
                return new ViewEngineResult(componentCreationResult.Component.View, this);
            }

            return new ViewEngineResult(componentCreationResult.SearchedLocations);
        }
    }
}