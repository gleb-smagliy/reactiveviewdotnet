﻿namespace ReactiveViewDotNet
{
    public class ReactiveViewEngineConfiguration
    {
        public ComponentsCachingOptions CachingOptions = ComponentsCachingOptions.NotCached;
    }
}